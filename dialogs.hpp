class RscTitles
{
	// cutRsc ["SXP_boss_intro", "PLAIN"]
	class SXP_boss_intro
	{
		idd = 20201219;
		duration = 5;
		fadeIn = 1;
		fadeOut = 1;
		
		class controls
		{
			class background_image: RscPicture
			{
				text = "media\boss.paa";
				style = 2096;
				x = (safeZoneX + (safeZoneW * 0.5)) - ((safeZoneH * 0.35)/(4/3));
				y = safeZoneY + (safeZoneH * 0.15);
				h = safeZoneH * 0.7;
				w = (safeZoneH * 0.7) / (4/3);
				colorText[] = {1,1,1,0.5};
			};
			class background: IGUIBACK
			{
				x = safeZoneX + (safeZoneW * 0.2);
				y = safeZoneY + (safeZoneH * 0.425);
				h = safeZoneH * 0.15;
				w = safeZoneW * 0.6;
				colorBackGround[] = {0,0,0,0.7};
			};
			class background_border: RscFrame
			{
				x = safeZoneX + (safeZoneW * 0.2);
				y = safeZoneY + (safeZoneH * 0.425);
				h = safeZoneH * 0.15;
				w = safeZoneW * 0.6;
				colorText[] = {0.969,0.961,0.22,1};
			};
			class caution: RscText
			{
				style = 2;
				x = safeZoneX + (safeZoneW * 0.2);
				y = safeZoneY + (safeZoneH * 0.415);
				h = safeZoneH * 0.1;
				w = safeZoneW * 0.6;
				colorText[] = {0.969,0.961,0.22,1};
				text = "-- CAUTION --";
				SizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 4)";
			};
			class boss: RscText
			{
				style = 2;
				x = safeZoneX + (safeZoneW * 0.2);
				y = safeZoneY + (safeZoneH * 0.51);
				h = safeZoneH * 0.05;
				w = safeZoneW * 0.6;
				text = "COLOSSUS";
				SizeEx = "(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 3)";
			};
		};
	};
};