// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord

//player createDiaryRecord ["Diary", ["Assets", "Example Mission Assets"]];
//player createDiaryRecord ["Diary", ["Mission", "Example Mission Briefing"]];
//player createDiaryRecord ["Diary", ["Situation", "Example Situation Briefing"]];

player createDiaryRecord ["Diary", ["Additional Info", "This has been a long process to bring this mission to the playable state that it's in today, and it's not one that I could have done alone. I would like to thank the following people for their help with this mission:
<br/> - LilDarth: Voice lines and naming.
<br/> - Ghost of Officer O'Mally: Naming, ideas, loading screen image, and organizing the TMTM Secret Santa.
<br/> - Ulysses: Mission name.
<br/> - TMTM: Giving me the inspiration to try crazy projects like this.
<br/><br/>Additionally, please read the notes below before starting the mission:
<br/> - For the full experience, please ensure that your ""Music"" and ""Radio"" volumes are both turned up.
<br/> - Your radios and map aren't going to work until you disable the communications jammer.
<br/> - The respawn position will automatically be moved forward as the mission progresses. JIPs will automatically be teleported to the respawn position.
<br/> - There are a few static searchlights around the map. These can be difficult to enter with the scroll menu, but ACE interacting with them should work every time.
<br/><br/>Have fun, and enjoy the 2020 TMTM Christmas op!"
]];

player createDiaryRecord ["Diary", ["Assets", "Due to the threat posed by Daybreaker, you will not have any heavy assets during this mission."]];

player createDiaryRecord ["Diary", ["Intel",
"While it is an extraordinarily powerful weapon, Daybreak is not suited for use against small ground targets. Due to its design as a long-range anti-aircraft weapon, it lacks the gun depression to hit anything except the largest ground based targets.
<br/>Because of this, you will need to approach Daybreaker on foot. We can't risk sending any heavy vehicles or aircraft into the region to assist you."
]];


player createDiaryRecord ["Diary", ["Mission",
"Blizzard Team, your mission today is to regain control of Daybreaker to prevent it from being used by the Anti-Gifters. Unfortunately due to the communications jamming, we don't have a lot of intel on the area at this time.
<br/><br/>That being said, we have pinpointed the location of the communications jammer. Your first objective will be to disable that communications jammer, allowing our long-range sensors to resume operation.
<br/><br/>Once the jammer has been disabled, HQ will provide you with additional instructions."
]];

player createDiaryRecord ["Diary", ["Situation",
"Early this morning, an extremist group of ""Anti-Gifters"" seized control of an experimental anti-air railgun system codenamed ""Daybreaker"".
<br/><br/>Our investigations have revealed that this group believes that Santa Claus is a ""CIA Hoax to spread tracking devices and chemtrails throughout the world"". Their plan is to use Daybreaker to shoot down Santa's sleigh.
<br/><br/>Shortly after they seized control of the railgun, all of our sensors in the region have gone dark. It's likely that a communications jammer has been deployed in the region, so we unfortunately don't have much intel."
]];
