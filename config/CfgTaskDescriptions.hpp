// Task Descriptions
// Can be used for moving task descriptions from script files to configs


// Create new classes under here. BIS_fnc_taskCreate will use either the class passed as the description parameter (in string format)
// The following would be loaded by setting the description to "example";
// If the description is blank (""), then the task ID will be used automatically
class example
{
	title = "Example Task Title";					// Title of task. Displayed as the task name
	description = "Example Task Description";		// Description of task. Additional details displayed when the task is selected on the map screen.
	marker = "";									// Task marker. Leave blank
};

class capture_fob
{
	title = "Disable communications jammer";
	description = "An enemy FOB is located within the exclusion zone. The anti-gifters have established a communications jammer at this FOB, which will disrupt your mapping systems and radio communications. Once the jammer has been disabled, HQ will be able to scan the area and direct you towards additional targets.<br/><br/>You should be able to reach the enemy FOB by proceeding south-east along the road.";
	marker = "";
};

class intel_west
{
	title = "Locate Access Codes";
	description = "We have located an outpost to the south-west, our intel suggests that you may be able to find one of the access codes there.";
	marker = "";
};

class intel_east
{
	title = "Locate Access Codes";
	description = "We have located an outpost to the south, our intel suggests that you may be able to find one of the access codes there.";
	marker = "";
};

class power_plant
{
	title = "Reactivate Power Plant";
	description = "To disrupt the ""wall generator"" we're going to need a lot of power. You'll need to figure out the interlock sequence to reactivate the power plant to the north-east.";
	marker = "";
};

class research_base
{
	title = "Redirect Power";
	description = "Once we have the access codes and have reactivated the power plant, go to the Research Station and redirect the excess power to the wall generator to fry it and disable the wall.";
	marker = "";
};

class capture_railgun
{
	title = "Capture Daybreaker";
	description = "The wall generator has been disabled, Daybreaker is in our sights. Secure it before the anti-gifters can fire it!";
	marker = "";
};

class boss_fight
{
	title = "Destroy the Colossus";
	description = "Eliminate the Colossus level threat to prevent the anti-gifters from reclaiming control over Daybreaker.";
	marker = "";
};

class boss_calibrate_gun
{
	title = "Recalibrate Daybreaker";
	description = "Daybreaker needs to be recalibrated before it can be fired. This can be done from the Calibration Center on the south-west side of compound.";
	marker = "";
};

class boss_rotate_gun
{
	title = "Aim Daybreaker";
	description = "The master control computer on top of Daybreaker's control center can be used to send targetting information to the gun. Aim Daybreaker at the Colossus.";
	marker = "";
};

class boss_fire_gun
{
	title = "Fire Daybreaker";
	description = "The time is now, fire that gun!";
	marker = "";
};

class boss_railgun_power
{
	title = "Transfer power to Daybreaker";
	description = "Activate the seven transfer switches to restore power to Daybreaker.";
	marker = "";
};