// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{	
	class example
	{
		displayName = "Example Loadout"; // Currently unused, basically just a human-readable name for the loadout
		
		// Weapon definitions all use the following format:
		// {Weapon Classname, Suppressor, Pointer (Laser/Flashlight), Optic, [Primary magazine, ammo count], [Secondary Magazine (GL), ammo count], Bipod}
		// Any empty definitions must be defined as an empty string, or an empty array. Otherwise the loadout will not apply correctly.
		
		primaryWeapon[] = {"arifle_MXC_F", "", "acc_flashlight", "optic_ACO", {"30Rnd_65x39_caseless_mag",30}, {}, ""}; // Primary weapon definition
		secondaryWeapon[] = {"launch_B_Titan_short_F", "", "", "", {"Titan_AP", 1}, {}, ""}; // Secondary weapon (Launcher) definition.
		handgunWeapon[] = {"hgun_ACPC2_F", "", "", "", {"9Rnd_45ACP_Mag", 9}, {}, ""}; // Handgun definition
		binocular = "Binocular";
		
		uniformClass = "U_B_CombatUniform_mcam_tshirt";
		headgearClass = "H_Watchcap_blk";
		facewearClass = "";
		vestClass = "V_Chestrig_khk";
		backpackClass = "B_AssaultPack_mcamo";
		
		// Linked items requires all six definitions to be present. Use empty strings if you do not want to add that item.
		linkedItems[] = {"ItemMap", "ItemGPS", "ItemRadio", "ItemCompass", "ItemWatch", ""}; // Linked items for the unit, must follow the order of: Map, GPS, Radio, Compass, Watch, NVGs.
		
		// When placed in an item array, magazines should also have their ammo count defined
		uniformItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4, 30}}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4, 30}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4, 30}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
	};
	
	class example_random
	{
		displayName = "Random Loadouts";
		class random_1
		{
			// Loadout info goes here
		};
		class random_2
		{
			// Loadout info goes here
		};
	};
	
	// Empty loadout with comments removed. Use this for your loadouts
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"", "", "", "", {}, {}, ""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"", "", "", "", {}, {}, ""};
		binocular = "";
		
		uniformClass = "";
		headgearClass = "";
		facewearClass = "";
		vestClass = "";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemGPS", "ItemRadio", "ItemCompass", "ItemWatch", "NVGoggles"};
		
		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	// All roles //
	// *Commander
	// *Command Medic
	// *Squad Leader
	// *Team Leader
	// *Medic
	// *Autorifle
	// Rifleman (HE)
	// *Rifleman
	
	// Squad Composition
	// SL
	// Medic
	// TL
	// Autorifle
	// Rifleman
	// TL
	// Rifleman (HE)
	// Rifleman
	
	class nato_base: base
	{
		displayName = "NATO Base";

		handgunWeapon[] = {"hgun_ACPC2_F","","acc_flashlight_pistol","",{"9Rnd_45ACP_Mag",8},{},""};
		binocular = "Binocular";

		uniformClass = "U_BG_Guerilla2_1";
		headgearClass = "H_HelmetB_light_black";
		facewearClass[] = {"CUP_G_ESS_BLK","CUP_G_ESS_BLK_Scarf_Grn","CUP_G_ESS_KHK_Scarf_Tan","CUP_G_ESS_BLK_Scarf_Face_Grn","CUP_G_ESS_KHK_Scarf_Face_Tan"};
		vestClass = "V_PlateCarrier1_blk";

		linkedItems[] = {"ItemMap","ItemMicroDAGR","TFAR_anprc152","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"ACE_EntrenchingTool",1},{"ItemcTabHCam",1},{"SmokeShellBlue",1,1}};
		
		basicMedUniform[] = {{"ACE_fieldDressing",10},{"ACE_epinephrine",2},{"ACE_morphine",1},{"ACE_tourniquet",2}};
	};
	
	class B_Soldier_F: nato_base
	{
		displayName = "Rifleman";
		
		primaryWeapon[] = {"CUP_arifle_CZ805_A1","","acc_flashlight","optic_Holosight_blk_F",{"CUP_30Rnd_TE1_Red_Tracer_556x45_CZ805",30},{},""};
		
		vestItems[] = {{"CUP_30Rnd_TE1_Red_Tracer_556x45_CZ805",8,30},{"MiniGrenade",2,1},{"SmokeShell",4,1},{"9Rnd_45ACP_Mag",2,8}};
	};
	
	class B_medic_F: B_Soldier_F
	{
		displayName = "Medic";
		
		facewearClass = "CUP_G_White_Scarf_Shades_GPS";
		backpackClass = "B_TacticalPack_blk";
		
		basicMedBackpack[] = {{"ACE_fieldDressing",80},{"ACE_epinephrine",25},{"ACE_adenosine",25},{"ACE_morphine",10},{"ACE_bloodIV",10}};
	};
	
	class cmd_medic: B_medic_F
	{
		displayName = "Command Medic";
		
		facewearClass[] = {"CUP_G_White_Scarf_Shades_GPSCombo_Beard","CUP_G_White_Scarf_Shades_GPSCombo_Beard_Blonde"};
		backpackClass = "B_RadioBag_01_black_F";
		
		linkedItems[] = {"ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ItemWatch",""};
		
		basicMedBackpack[] = {{"ACE_fieldDressing",80},{"ACE_epinephrine",15},{"ACE_adenosine",10},{"ACE_morphine",5},{"ACE_bloodIV",5}};
	};
	
	class B_Soldier_TL_F: B_Soldier_F
	{
		displayName = "Team Leader";

		primaryWeapon[] = {"CUP_arifle_CZ805_GL","","acc_flashlight","optic_Holosight_blk_F",{"CUP_30Rnd_TE1_Red_Tracer_556x45_CZ805",30},{"1Rnd_HE_Grenade_shell",1},""};

		facewearClass[] = {"CUP_G_Grn_Scarf_Shades_GPSCombo","CUP_G_Tan_Scarf_Shades_GPSCombo"};
		backpackClass = "B_AssaultPack_blk";

		backpackItems[] = {{"1Rnd_HE_Grenade_shell",16,1},{"CUP_1Rnd_StarFlare_Green_M203",10,1},{"CUP_1Rnd_StarCluster_Green_M203",3,1}};
	};
	
	class B_Soldier_SL_F: B_Soldier_TL_F
	{
		displayName = "Squad Leader";

		backpackClass = "B_RadioBag_01_black_F";

		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_anprc152","ItemCompass","ItemWatch",""};
	};
	
	class B_Soldier_AR_F: nato_base
	{
		displayName = "B_Soldier_F";

		primaryWeapon[] = {"LMG_Mk200_black_F","","acc_flashlight","optic_Holosight_blk_F",{"200Rnd_65x39_cased_Box_Tracer_Red",200},{},"bipod_01_F_blk"};
		
		backpackClass = "B_AssaultPack_blk";
		
		vestItems[] = {{"MiniGrenade",2,1},{"SmokeShell",4,1},{"9Rnd_45ACP_Mag",2,8},{"200Rnd_65x39_cased_Box_Tracer_Red",2,200}};
		backpackItems[] = {{"200Rnd_65x39_cased_Box_Tracer_Red",2,200}};
	};
	class B_Officer_F: B_Soldier_TL_F
	{
		displayName = "Commmander";
		
		backpackClass = "B_RadioBag_01_black_F";
		facewearClass[] = {"CUP_G_Grn_Scarf_Shades_GPSCombo_Beard_Blonde","CUP_G_Grn_Scarf_Shades_GPSCombo_Beard","CUP_G_Tan_Scarf_Shades_GPSCombo_Beard_Blonde","CUP_G_Tan_Scarf_Shades_GPSCombo_Beard"};
		
		linkedItems[] = {"ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ItemWatch",""};
	};
};