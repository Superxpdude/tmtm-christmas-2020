// Function for updating mission tasks when objectives are completed.
// Only execute on the server. Tasks should only be created server-side.
if (!isServer) exitWith {};

// Code for task updates goes into these fields. Can be any code required, including new task creation, task state updates, etc.
switch (toLower (_this select 0)) do {
	case "fob_captured": {
		if ("task_fob" call BIS_fnc_taskCompleted) exitWith {};
		["task_fob", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		
		"respawn_west" setMarkerPos fob_respawn_pos;
		
		[true] remoteExec ["SXP_fnc_enableRadios", 0];
		
		{
			_x setMarkerAlpha 0;
		} forEach ((getMissionLayerEntities "Map Cover Phase 1") select 1);
		
		// Create our additional tasks
		[
			true,
			"task_intel_west",
			"intel_west",
			intel_west_location,
			"CREATED",
			30,
			true,
			"download",
			true
		] call BIS_fnc_taskCreate;
		
		[
			true,
			"task_intel_east",
			"intel_east",
			intel_east_location,
			"CREATED",
			25,
			true,
			"download",
			true
		] call BIS_fnc_taskCreate;
		
		[
			true,
			"task_power_plant",
			"power_plant",
			power_plant_location,
			"CREATED",
			20,
			true,
			"use",
			true
		] call BIS_fnc_taskCreate;
		
		[
			true,
			"task_research_base",
			"research_base",
			research_base_location,
			"CREATED",
			15,
			true,
			"download",
			true
		] call BIS_fnc_taskCreate;
		
		// Play the radio lines
		[0, {
			{
				player_hq_unit sideRadio _x;
			} forEach ["vo_comms_01","vo_comms_02","vo_comms_03","vo_comms_04","vo_comms_05","vo_comms_06","vo_comms_07","vo_comms_08","vo_comms_09"];
		}] remoteExec ["BIS_fnc_call", 0];
	};
	
	case "intel_west_activated": {
		if ("task_intel_west" call BIS_fnc_taskCompleted) exitWith {};
		["task_intel_west", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		[player_hq_unit,"vo_access_bag"] remoteExec ["sideRadio", 0];
	};
	
	case "intel_east_activated": {
		if ("task_intel_east" call BIS_fnc_taskCompleted) exitWith {};
		["task_intel_east", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		[player_hq_unit,"vo_access_ango"] remoteExec ["sideRadio", 0];
	};
	
	case "power_plant_activated": {
		if ("task_power_plant" call BIS_fnc_taskCompleted) exitWith {};
		["task_power_plant", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		[player_hq_unit,"vo_power_plant"] remoteExec ["sideRadio", 0];
		
		// Lights flickering back on
		// TODO: Rewrite this to be more easily expandible
		[] spawn {
			private _lightArr = ((getMissionLayerEntities "Power Plant Lamps") select 0);
			{[_x,"on"] remoteExec ["switchLight", 0];} forEach _lightArr;
			sleep 0.3;
			{[_x,"off"] remoteExec ["switchLight", 0];} forEach _lightArr;
			sleep 2;
			{[_x,"on"] remoteExec ["switchLight", 0];} forEach _lightArr;
			sleep 0.5;
			{[_x,"off"] remoteExec ["switchLight", 0];} forEach _lightArr;
			sleep 1;
			{[_x,"on"] remoteExec ["switchLight", 0];} forEach _lightArr;
			sleep 1;
			{[_x,"off"] remoteExec ["switchLight", 0];} forEach _lightArr;
			sleep 0.5;
			{[_x,"on"] remoteExec ["switchLight", 0];} forEach _lightArr;
		};
	};
	
	case "research_base_activated": {
		if ("task_research_base" call BIS_fnc_taskCompleted) exitWith {};
		["task_research_base", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		
		// Remove the defensive wall
		[] spawn {
			[player_hq_unit,"vo_research_base"] remoteExec ["sideRadio", 0];
			sleep 7;
			private _wallArr = (((getMissionLayerEntities "Defensive Wall") select 0) call BIS_fnc_arrayShuffle);
			private _grp = createGroup civilian;
			{
				private _nul = _grp createUnit ["ModuleLightning_F", [(getPosATL _x) select 0, (getPosATL _x) select 1], [], 0, "FORM"];
				deleteVehicle _x;
				sleep 0.2;
				[_nul] spawn {
					sleep 10;
					deleteVehicle (_this select 0);
				};
			} forEach _wallArr;
			_grp deleteGroupWhenEmpty true;
			
			sleep 3;
			
			{
				_x setMarkerAlpha 0;
			} forEach ((getMissionLayerEntities "Map Cover Final") select 1);
			
			[player_hq_unit,"vo_wall_down"] remoteExec ["sideRadio", 0];
			
			"respawn_west" setMarkerPos mid_respawn_pos;
			
			[
				true,
				"task_capture_railgun",
				"capture_railgun",
				railgun_base,
				"ASSIGNED",
				10,
				true,
				"attack",
				true
			] call BIS_fnc_taskCreate;
		};
	};
	
	case "railgun_captured": {
		if ("task_capture_railgun" call BIS_fnc_taskCompleted) exitWith {};
		["task_capture_railgun", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		
		"respawn_west" setMarkerPos railgun_respawn_pos;
		
		// Clean up any extra enemy units before the boss fight starts
		{
			_x setDamage 1;
		} forEach (allUnits select {((side _x) == independent) && (!isPlayer _x)});
		
		// Heal everyone
		[0, {[player, player] call ace_medical_treatment_fnc_fullHeal;}] remoteExec ["BIS_fnc_call", 0];
		
		[] spawn {
			[player_hq_unit,"vo_railgun_01"] remoteExec ["sideRadio", 0];
			sleep 12;
			[player_hq_unit,"vo_railgun_02"] remoteExec ["sideRadio", 0];
			sleep 15;
			[player_hq_unit,"vo_railgun_03"] remoteExec ["sideRadio", 0];
			sleep 6;
			[player_hq_unit,"vo_railgun_04"] remoteExec ["sideRadio", 0];
			sleep 6;
			[player_hq_unit,"vo_railgun_05"] remoteExec ["sideRadio", 0];
			
			// Accelerate time
			[] spawn {
				setTimeMultiplier 120;
				waitUntil {dayTime >= 20};
				setTimeMultiplier 0.1;
			};
			
			// Boss fight loop
			[] spawn {
				missionNamespace setVariable ["SXP_boss_music_active", true, true];
				["wingman_peacekeeper_2"] remoteExec ["playMusic", 0];
				sleep 4.5;
				[["SXP_boss_intro", "PLAIN"]] remoteExec ["cutRsc", 0];
				sleep 3;
				private _bossLightningArray = ((getMissionLayerEntities "Boss Spawn Lightnings") select 0);
				private _lightningGrp = createGroup civilian;
				{
					private _nul = _lightningGrp createUnit ["ModuleLightning_F", [(getPosATL _x) select 0, (getPosATL _x) select 1], [], 0, "FORM"];
					[_nul] spawn {
						sleep 10;
						deleteVehicle (_this select 0);
					};
					sleep 0.3;
				} forEach (_bossLightningArray call BIS_fnc_arrayShuffle);
				sleep 1;
				{
					private _nul = _lightningGrp createUnit ["ModuleLightning_F", [(getPosATL _x) select 0, (getPosATL _x) select 1], [], 0, "FORM"];
					[_nul] spawn {
						sleep 10;
						deleteVehicle (_this select 0);
					};
				} forEach (_bossLightningArray call BIS_fnc_arrayShuffle);
				_lightningGrp deleteGroupWhenEmpty true;
				{
					_x hideObjectGlobal false;
				} forEach ((getMissionLayerEntities "Boss Objects") select 0);
				// Play the boss intro sound
				playSound3D [getMissionPath "media\sounds\boss.ogg", boss_object, false, getPosASL boss_object, 5, 1, 10000];
				sleep 10;
				[] call SXP_fnc_bossAttacks;
				private _bossUnits = [];
				while {missionNamespace getVariable ["SXP_boss_fight_active", false]} do {
					private _lightningGrp = createGroup civilian;
					private _spawnPositions = (((getMissionLayerEntities "Boss Fight Spawn Locations") select 0) call BIS_fnc_arrayShuffle);
					_spawnPositions resize (((ceil ((count allPlayers) / 8)) max 3) min (count _spawnPositions));
					{
						private _nul = _lightningGrp createUnit ["ModuleLightning_F", [(getPosATL _x) select 0, (getPosATL _x) select 1], [], 0, "FORM"];
						[_nul] spawn {
							sleep 10;
							deleteVehicle (_this select 0);
						};
						sleep 0.5;
						private _grp = [getPos _x, independent, (configFile >> "CfgGroups" >> "Indep" >> "CUP_I_NAPA" >> "Infantry" >> "CUP_I_NAPA_InfSquad")] call BIS_fnc_spawnGroup;
						_grp deleteGroupWhenEmpty true;
						{
							_x setSkill ["aimingAccuracy", 0];
						} forEach (units _grp);
						private _wp = _grp addWaypoint [railgun_base, 20];
						_wp setWayPointType "MOVE";
						_grp setBehaviour "AWARE";
						{
							_x addCuratorEditableObjects [units _grp, true];
						} forEach allCurators;
						_bossUnits append (units _grp);
					} forEach _spawnPositions;
					waitUntil {({alive _x} count _bossUnits) <= 16};
				};
			};
		
			// TODO: Add space in here for the "BOSS FIGHT" dialogue to happen
			[
				true,
				"task_boss_fight",
				"boss_fight",
				objNull,
				"ASSIGNED",
				100,
				true,
				"danger",
				true
			] call BIS_fnc_taskCreate;
			
			sleep 75;
			
			[player_hq_unit,"vo_plan_01"] remoteExec ["sideRadio", 0];
			sleep 5;
			[player_hq_unit,"vo_plan_02"] remoteExec ["sideRadio", 0];
			sleep 6;
			[player_hq_unit,"vo_plan_03"] remoteExec ["sideRadio", 0];
			sleep 5;
			[player_hq_unit,"vo_plan_04"] remoteExec ["sideRadio", 0];
			/*
			[
				true,
				["task_calibrate_railgun","task_boss_fight"],
				"boss_calibrate_gun",
				boss_calibrate_laptop,
				"CREATED",
				90,
				true,
				"rearm",
				true
			] call BIS_fnc_taskCreate;
			[
				true,
				["task_aim_railgun","task_boss_fight"],
				"boss_rotate_gun",
				boss_railgun_laptop,
				"CREATED",
				95,
				true,
				"target",
				true
			] call BIS_fnc_taskCreate;
			*/
			[true,["task_railgun_power_master","task_boss_fight"],"boss_railgun_power",objNull,"CREATED",81,true,"use",true] call BIS_fnc_taskCreate;
			[true,["task_railgun_power_1","task_railgun_power_master"],"boss_railgun_power",railgun_power_sw_1,"CREATED",80,false,"use",true] call BIS_fnc_taskCreate;
			[true,["task_railgun_power_2","task_railgun_power_master"],"boss_railgun_power",railgun_power_sw_2,"CREATED",80,false,"use",true] call BIS_fnc_taskCreate;
			[true,["task_railgun_power_3","task_railgun_power_master"],"boss_railgun_power",railgun_power_sw_3,"CREATED",80,false,"use",true] call BIS_fnc_taskCreate;
			[true,["task_railgun_power_4","task_railgun_power_master"],"boss_railgun_power",railgun_power_sw_4,"CREATED",80,false,"use",true] call BIS_fnc_taskCreate;
			[true,["task_railgun_power_5","task_railgun_power_master"],"boss_railgun_power",railgun_power_sw_5,"CREATED",80,false,"use",true] call BIS_fnc_taskCreate;
			[true,["task_railgun_power_6","task_railgun_power_master"],"boss_railgun_power",railgun_power_sw_6,"CREATED",80,false,"use",true] call BIS_fnc_taskCreate;
			[true,["task_railgun_power_7","task_railgun_power_master"],"boss_railgun_power",railgun_power_sw_7,"CREATED",80,false,"use",true] call BIS_fnc_taskCreate;
		};
	};
	
	case "railgun_power_1": {
		if ("task_railgun_power_1" call BIS_fnc_taskCompleted) exitWith {};
		["task_railgun_power_1", "SUCCEEDED", false] call BIS_fnc_taskSetState;
		[railgun_power_light_1,"off"] remoteExec ["switchLight", 0, railgun_power_light_1];
		["railgun_power_complete"] call SXP_fnc_updateTask;
	};
	
	case "railgun_power_2": {
		if ("task_railgun_power_2" call BIS_fnc_taskCompleted) exitWith {};
		["task_railgun_power_2", "SUCCEEDED", false] call BIS_fnc_taskSetState;
		[railgun_power_light_2,"off"] remoteExec ["switchLight", 0, railgun_power_light_2];
		["railgun_power_complete"] call SXP_fnc_updateTask;
	};
	
	case "railgun_power_3": {
		if ("task_railgun_power_3" call BIS_fnc_taskCompleted) exitWith {};
		["task_railgun_power_3", "SUCCEEDED", false] call BIS_fnc_taskSetState;
		[railgun_power_light_3,"off"] remoteExec ["switchLight", 0, railgun_power_light_3];
		["railgun_power_complete"] call SXP_fnc_updateTask;
	};
	
	case "railgun_power_4": {
		if ("task_railgun_power_4" call BIS_fnc_taskCompleted) exitWith {};
		["task_railgun_power_4", "SUCCEEDED", false] call BIS_fnc_taskSetState;
		[railgun_power_light_4,"off"] remoteExec ["switchLight", 0, railgun_power_light_4];
		["railgun_power_complete"] call SXP_fnc_updateTask;
	};
	
	case "railgun_power_5": {
		if ("task_railgun_power_5" call BIS_fnc_taskCompleted) exitWith {};
		["task_railgun_power_5", "SUCCEEDED", false] call BIS_fnc_taskSetState;
		[railgun_power_light_5,"off"] remoteExec ["switchLight", 0, railgun_power_light_5];
		["railgun_power_complete"] call SXP_fnc_updateTask;
	};
	
	case "railgun_power_6": {
		if ("task_railgun_power_6" call BIS_fnc_taskCompleted) exitWith {};
		["task_railgun_power_6", "SUCCEEDED", false] call BIS_fnc_taskSetState;
		[railgun_power_light_6,"off"] remoteExec ["switchLight", 0, railgun_power_light_6];
		["railgun_power_complete"] call SXP_fnc_updateTask;
	};
	
	case "railgun_power_7": {
		if ("task_railgun_power_7" call BIS_fnc_taskCompleted) exitWith {};
		["task_railgun_power_7", "SUCCEEDED", false] call BIS_fnc_taskSetState;
		[railgun_power_light_7,"off"] remoteExec ["switchLight", 0, railgun_power_light_7];
		["railgun_power_complete"] call SXP_fnc_updateTask;
	};
	
	case "railgun_power_complete": {
		if (({!(_x call BIS_fnc_taskCompleted)} count ["task_railgun_power_1","task_railgun_power_2","task_railgun_power_3","task_railgun_power_4","task_railgun_power_5","task_railgun_power_6","task_railgun_power_7"]) <= 0) then {
			["task_railgun_power_master", "SUCCEEDED", true] call BIS_fnc_taskSetState;
			[0, {[player, player] call ace_medical_treatment_fnc_fullHeal;}] remoteExec ["BIS_fnc_call", 0];
			railgun_light hideObjectGlobal false;
			[player_hq_unit,"vo_railgun_power"] remoteExec ["sideRadio", 0];
			[
				true,
				["task_calibrate_railgun","task_boss_fight"],
				"boss_calibrate_gun",
				boss_calibrate_laptop,
				"CREATED",
				90,
				true,
				"rearm",
				true
			] call BIS_fnc_taskCreate;
			["boss_next_stage"] call SXP_fnc_updateTask;
			["railgun_ready"] call SXP_fnc_updateTask;
		};
	};
	
	case "railgun_calibrated": {
		if ("task_calibrate_railgun" call BIS_fnc_taskCompleted) exitWith {};
		["task_calibrate_railgun", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		[0, {[player, player] call ace_medical_treatment_fnc_fullHeal;}] remoteExec ["BIS_fnc_call", 0];
		[player_hq_unit,"vo_railgun_calibrated"] remoteExec ["sideRadio", 0];
		[
			true,
			["task_aim_railgun","task_boss_fight"],
			"boss_rotate_gun",
			boss_railgun_laptop,
			"CREATED",
			95,
			true,
			"target",
			true
		] call BIS_fnc_taskCreate;
		["boss_next_stage"] call SXP_fnc_updateTask;
		["railgun_ready"] call SXP_fnc_updateTask;
	};
	
	case "railgun_aimed": {
		if ("task_aim_railgun" call BIS_fnc_taskCompleted) exitWith {};
		["task_aim_railgun", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		[0, {[player, player] call ace_medical_treatment_fnc_fullHeal;}] remoteExec ["BIS_fnc_call", 0];
		[player_hq_unit,"vo_railgun_aimed"] remoteExec ["sideRadio", 0];
		["boss_next_stage"] call SXP_fnc_updateTask;
		["railgun_ready"] call SXP_fnc_updateTask;
	};
	
	case "railgun_ready": {
		if (({!(_x call BIS_fnc_taskCompleted)} count ["task_calibrate_railgun","task_aim_railgun","task_railgun_power_master"]) <= 0) then {
			[player_hq_unit,"vo_railgun_ready"] remoteExec ["sideRadio", 0];
			[
				true,
				["task_fire_railgun","task_boss_fight"],
				"boss_fire_gun",
				boss_railgun_laptop,
				"ASSIGNED",
				150,
				true,
				"kill",
				true
			] call BIS_fnc_taskCreate;
		};
	};
	
	case "boss_next_stage": {
		private _currentStage = missionNamespace getVariable ["SXP_boss_fight_phase", 1];
		private _newStage = (_currentStage + 1) min 3;
		missionNamespace setVariable ["SXP_boss_fight_phase", _newStage, true];
	};
	
	case "railgun_fired": {
		if ("task_fire_railgun" call BIS_fnc_taskCompleted) exitWith {};
		[] spawn {
			if ("task_fire_railgun" call BIS_fnc_taskCompleted) exitWith {};
			missionNamespace setVariable ["SXP_boss_fight_active", false, true];
			["task_fire_railgun", "SUCCEEDED", true] call BIS_fnc_taskSetState;
			[0, {[player, player] call ace_medical_treatment_fnc_fullHeal;}] remoteExec ["BIS_fnc_call", 0];
			{[_x,"off"] remoteExec ["switchLight", 0];} forEach ((getMissionLayerEntities "Railgun Base Lights") select 0);
			{
				[_x, {_this allowDamage true; _this setHitPointDamage ["#light", 1]}] remoteExec ["BIS_fnc_call", _x];
			} forEach ((getMissionLayerEntities "Final Base Searchlights") select 0);
			[railgun_base,["railgun_firing", 10000, 1, false]] remoteExec ["say3D", 0];
			sleep 20;
			[[], {cutText ["", "WHITE OUT", 5, true]; 5 fadeMusic 0; sleep 5; playMusic "";}] remoteExec ["BIS_fnc_spawn", 0];
			sleep 5;
			// Kill all enemy units once the gun fires
			{
				_x setDamage 1;
			} forEach (allUnits select {((side _x) == independent) && (!isPlayer _x)});
			// Remove player ammo and set TFAR volume to 0
			[0,{player setVehicleAmmo 0; player setVariable ["tf_globalVolume", 0]}] remoteExec ["BIS_fnc_call", 0];
			sleep 2;
			["task_boss_fight", "SUCCEEDED", true] call BIS_fnc_taskSetState;
			sleep 3;
			["victory", true, true, false] remoteExec ["BIS_fnc_endMission", 0];
		};
	};
};