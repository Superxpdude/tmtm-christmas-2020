// Boss attack function
// Handles periodically attacking the player base while they are trying to use the railgun
// Spawns two threads. One for lighting, another for rockets.
// Only to be run on the server.

if (!isServer) exitWith {};

// Mark the boss fight as "active"
missionNamespace setVariable ["SXP_boss_fight_active", true, true];
missionNamespace setVariable ["SXP_boss_fight_phase", 1, true];

// Spawn our rocket thread.
[] spawn {
	private _nextAttackTime = cba_missionTime + 30;
	waitUntil {cba_missionTime >= _nextAttackTime};
	while {missionNamespace getVariable ["SXP_boss_fight_active", false]} do {
		private _targetArray = ((getMissionLayerEntities "Final Boss Bomb Points") select 0) call BIS_fnc_arrayShuffle;
		private _targetCount = switch (missionNamespace getVariable ["SXP_boss_fight_phase", 1]) do {
			case 1: {2};
			case 2: {4};
			case 3: {8};
			default {1};
		};
		_targetArray resize _targetCount;
		{
			[_x] spawn {
				private _targetObj = _this select 0;
				private _targetPos = getPosATL _targetObj;
				private _lightPos = getPosATL _targetObj;
				_lightPos set [2,10];
				private _bombPos = getPosATL _targetObj;
				_bombPos set [2,100];
				private _markerPos = getPosATL _targetObj;
				_markerPos set [2,3];
				private _lightObj = createVehicle ["Reflector_Cone_01_narrow_red_F", _lightPos, [], 0, "CAN_COLLIDE"];
				_lightObj setVectorDirAndUp [[0,0,-1], [0,1,0]];
				private _markerObj = createVehicle ["Sign_Arrow_Large_F", _markerPos, [], 0, "CAN_COLLIDE"];
				playSound3D ["A3\Sounds_F\sfx\alarm.wss", _targetObj, false, getPosASL _targetObj, 3, 1, 300];
				sleep 10;
				private _bomb = createVehicle ["R_TBG32V_F", _bombPos, [], 0, "CAN_COLLIDE"];
				_bomb setVectorDirAndUp [[0,0,-1], [0,1,0]];
				waitUntil {!alive _bomb};
				{
					deleteVehicle _x;
				} forEach [_lightObj, _markerObj];
			};
			sleep 0.5
		} forEach _targetArray;
		
		_nextAttackTime = cba_missionTime + (random [25,35,45]);
		waitUntil {cba_missionTime >= _nextAttackTime};
	};
};

// Spawn our lightning thread
[] spawn {
	private _nextLightningTime = cba_missionTime + 5;
	waitUntil {cba_missionTime >= _nextLightningTime};
	while {missionNamespace getVariable ["SXP_boss_fight_active", false]} do {
		private _targetArray = ((getMissionLayerEntities "Final Boss Lightning Points") select 0) call BIS_fnc_arrayShuffle;
		private _targetCount = (floor (random 3)) + 1; // Max of three at a time
		_targetArray resize _targetCount;
		private _grp = createGroup civilian;
		{
			// Create a lightning bolt module
			private _nul = _grp createUnit ["ModuleLightning_F", getPosATL _x, [], 0, "FORM"];
			sleep 0.5;
			[_nul] spawn {
				sleep 10;
				deleteVehicle (_this select 0);
			};
		} forEach _targetArray;
		_grp deleteGroupWhenEmpty true;
		
		private _timeOut = switch (missionNamespace getVariable ["SXP_boss_fight_phase", 1]) do {
			case 1: {[25,30,35]};
			case 2: {[10,18,25]};
			case 3: {[5,7,10]};
			default {[25,25,25]};
		};
		
		_nextLightningTime = cba_missionTime + (random _timeOut);
		waitUntil {cba_missionTime >= _nextLightningTime};
	};
};