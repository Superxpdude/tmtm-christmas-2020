/*
	SXP_fnc_lightsout
	Author: Superxpdude
	
	Allows for a multiplayer-synchronized version of the "lights out" puzzle to be used in Arma.
	Switch objects can be initialized at any location on the map, and will toggle the appropriate light as needed.
	
	Must be set to "postInit" in CfgFunctions in order to work properly.
	
	Parameters:
		0: String - Mode of operation. Used to distinguish between initialization, server, and client operation.

	Returns: Nothing
*/

params [
	"_mode", 
	"_params"
];

//systemChat format ["LightsOut running: %1", _this];

// Check the mode of operation
switch (toLower _mode) do {	
	case "postinit": {
		//systemChat "LightsOut postInit running";
		// Post-Initialization
		// Handles randomization of switches, and initialization of variables
		if (isServer) then {
			//systemChat "LightsOut server postInit running";
			// Handle random IDs for switches (and their associated lights)
			private _swNumbers = [1,2,3,4,5,6,7,8,9];
			{
				private _index = floor (random (count _swNumbers));
				private _id = _swNumbers select _index;
				(_x select 0) setVariable ["pp_lightsOut_sw_id", _id, true];
				(_x select 1) setVariable ["pp_lightsOut_light_id", _id, true];
				_swNumbers deleteAt _index;
			} forEach [
				[pp_lightsOut_sw_1,pp_lightsOut_swLight_1],
				[pp_lightsOut_sw_2,pp_lightsOut_swLight_2],
				[pp_lightsOut_sw_3,pp_lightsOut_swLight_3],
				[pp_lightsOut_sw_4,pp_lightsOut_swLight_4],
				[pp_lightsOut_sw_5,pp_lightsOut_swLight_5],
				[pp_lightsOut_sw_6,pp_lightsOut_swLight_6],
				[pp_lightsOut_sw_7,pp_lightsOut_swLight_7],
				[pp_lightsOut_sw_8,pp_lightsOut_swLight_8],
				[pp_lightsOut_sw_9,pp_lightsOut_swLight_9]
			];
			
			// Set some variables on the "grid" lights in the power plant
			pp_lamp_a1 setVariable ["pp_lightsOut_light_id", 1, true];
			pp_lamp_b1 setVariable ["pp_lightsOut_light_id", 2, true];
			pp_lamp_c1 setVariable ["pp_lightsOut_light_id", 3, true];
			pp_lamp_a2 setVariable ["pp_lightsOut_light_id", 4, true];
			pp_lamp_b2 setVariable ["pp_lightsOut_light_id", 5, true];
			pp_lamp_c2 setVariable ["pp_lightsOut_light_id", 6, true];
			pp_lamp_a3 setVariable ["pp_lightsOut_light_id", 7, true];
			pp_lamp_b3 setVariable ["pp_lightsOut_light_id", 8, true];
			pp_lamp_c3 setVariable ["pp_lightsOut_light_id", 9, true];
			
			// Set a variable on the reset switch
			pp_switch_reset setVariable ["pp_lightsOut_sw_id", -1, true];
			
			// Create our array to handle synchronizing light states. Should be nine entries of "false" at mission start.
			pp_lightsOut_array = [false,false,false,false,false,false,false,false,false];
			publicVariable "pp_lightsOut_array";
			
			missionNamespace setVariable ["pp_lightsOut_enabled", true, true];
			
			//systemChat format ["LightsOut array set to %1",pp_lightsOut_array];
			
			missionNamespace setVariable ["pp_lights_array", ((getMissionLayerEntities "Power Plant Grid Lamps") select 0) + ((getMissionLayerEntities "Power Plant Transformer Lamps") select 0), true];
			missionNamespace setVariable ["pp_switches_array", ((getMissionLayerEntities "Power Plant Switches") select 0), true];
		};
		
		// Handles setting up holdActions, and initializing the light state for each client.
		// Spawn this part since we'll need to wait until the lightsout array is defined.
		// Added holdactions to this, since getMissionLayerEntities only works on the server.
		[] spawn {
			waitUntil {!isNil "pp_switches_array"};
			{
				[
					_x,
					"Toggle power switch",
					"media\holdActions\holdAction_power.paa",
					"media\holdActions\holdAction_power.paa",
					"((_target getVariable ['pp_lightsOut_sw_id',0]) > 0) && ((_this distance _target) < 5) && (missionNamespace getVariable ['pp_lightsOut_enabled',false]) && ('task_power_plant' call BIS_fnc_taskExists)",
					"((_target getVariable ['pp_lightsOut_sw_id',0]) > 0) && ((_caller distance _target) < 5) && (missionNamespace getVariable ['pp_lightsOut_enabled',false]) && ('task_power_plant' call BIS_fnc_taskExists)",
					{},
					{},
					{["serverUpdate",_target getVariable ["pp_lightsOut_sw_id",0]] remoteExec ["SXP_fnc_lightsOut", 2];},
					{},
					[],
					2,
					100,
					false
				] call BIS_fnc_holdActionAdd;
				
				[
					_x,
					"Pull reset switch",
					"media\holdActions\holdAction_power.paa",
					"media\holdActions\holdAction_power.paa",
					"((_target getVariable ['pp_lightsOut_sw_id',0]) == -1) && ((_this distance _target) < 5) && (missionNamespace getVariable ['pp_lightsOut_enabled',false]) && ('task_power_plant' call BIS_fnc_taskExists)",
					"((_target getVariable ['pp_lightsOut_sw_id',0]) == -1) && ((_caller distance _target) < 5) && (missionNamespace getVariable ['pp_lightsOut_enabled',false]) && ('task_power_plant' call BIS_fnc_taskExists)",
					{},
					{},
					{["serverReset"] remoteExec ["SXP_fnc_lightsOut", 2];},
					{},
					[],
					5,
					200,
					false
				] call BIS_fnc_holdActionAdd;
			} forEach pp_switches_array;
			
			//systemChat "LightsOut spawned postInit client update";
			waitUntil {!isNil "pp_lightsOut_array"}; // Wait until the array is defined.
			//systemChat "LightsOut postInit client update called";
			["clientUpdate"] call SXP_fnc_lightsOut; // Update the light state on all clients.
		};
	};
	
	case "clientupdate": {
		//systemChat "LightsOut client update running";
		// Updates the light status on all clients.
		// This might be a bit slow, but we don't need to execute it very often.
		
		waitUntil {!isNil "pp_lights_array"};
		private _lightsArray = + pp_lights_array;
		
		private _lightsoutArr = if (!isNil "_params") then {
			_params
		} else {
			pp_lightsOut_array
		};
		
		//systemChat format ["LightsOut client update using array %1", _lightsoutArr];
		
		{
			
			private _lightState = _x;
			private _lightID = _forEachIndex + 1;
			{
				if ((_x getVariable ["pp_lightsOut_light_id", 0]) == _lightID) then {
					_x switchLight (["off","on"] select _lightState);
				};
			} forEach _lightsArray; // Replace this with array for all lights
		} forEach _lightsoutArr; // This array will get updated with the light state for each light, and is synchronized across all clients.
	};
	
	case "serverupdate": {
		// Updates the light states on the server.
		// Only execute on the server
		if (isServer AND (missionNamespace getVariable ["pp_lightsOut_enabled", false])) then {
			//systemChat format ["LightsOut server update running for %1", _params];
			// Define an array of other lights that must be changed upon a "master" light changing
			// Light IDs are as follows
			//		1, 2, 3
			//		4, 5, 6
			//		7, 8, 9
			// Indexes are (-1) to the light ID. So LightID 1 would be LightIndex 0 (for working with arrays)
			private _subLightArray = [
				[2,4],[1,3,5],[2,6],
				[1,5,7],[2,4,6,8],[3,5,9],
				[4,8],[5,7,9],[6,8]
			];
			
			private _masterLightID = _params; // Get the ID of the light
			private _lightIndexes = [_masterLightID];
			_lightIndexes append (_subLightArray select (_masterLightID - 1));
			_lightIndexes = _lightIndexes apply {_x - 1};
			
			{
				private _lightState = pp_lightsOut_array select _x;
				// Swap the light state of that light
				if (_lightState) then {
					pp_lightsOut_array set [_x, false];
				} else {
					pp_lightsOut_array set [_x, true];
				};
			} forEach _lightIndexes;
			// Check to see if the puzzle has been solved
			if (({_x} count pp_lightsOut_array) == (count pp_lightsOut_array)) then {
				// Mark the puzzle as "disabled" since it has been completed
				missionNamespace setVariable ["pp_lightsOut_enabled", false, true];
				// TODO: Place task update code here
				["power_plant_activated"] call SXP_fnc_updateTask;
			};
			
			// Push the light status to all clients.
			publicVariable "pp_lightsOut_array";
			
			//systemChat format ["LightsOut server update array %1", pp_lightsOut_array];
			// Update the light status on all clients
			["clientUpdate", pp_lightsOut_array] remoteExec ["SXP_fnc_lightsOut", 0];
		};
	};
	
	case "serverreset": {
		// Reset the puzzle to its initial state
		if (isServer) then {
			pp_lightsOut_array = [false,false,false,false,false,false,false,false,false];
			publicVariable "pp_lightsOut_array";
			
			// Update the light status on all clients
			["clientUpdate", pp_lightsOut_array] remoteExec ["SXP_fnc_lightsOut", 0];
		};
	};
};