// Function to rotate the railgun
// Only to be run on the server
// MUST BE SPAWNED
if (!isServer) exitWith {};
if (missionNamespace getVariable ["railgun_rotating", false]) exitWith {};
// Tell all clients that the gun has started moving
missionNamespace setVariable ["railgun_rotating", true, true];

// Move the gun
for "_i" from 359 to 220 step -1 do {
	[railgun_base,_i] remoteExec ["setDir", 0];
	sleep 1;
};

["railgun_aimed"] call SXP_fnc_updateTask;