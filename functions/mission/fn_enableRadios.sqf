// Function to enable or disable player radios.
// Takes one paramter for enabled or disabled.
params ["_enabled"];

if (_enabled) then {
	player setVariable ["tf_receivingDistanceMultiplicator", 1];
	player setVariable ["tf_sendingDistanceMultiplicator", 1];
} else {
	player setVariable ["tf_receivingDistanceMultiplicator", 0];
	player setVariable ["tf_sendingDistanceMultiplicator", 0];
};