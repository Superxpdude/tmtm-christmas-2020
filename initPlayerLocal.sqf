// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point

// Uncomment the line below to use briefing.sqf for mission briefings. Un-needed if you're using XPTBriefings.hpp
[] execVM "scripts\briefing.sqf";

if !("task_power_plant" call BIS_fnc_taskCompleted) then {
	{_x switchLight "off"} forEach ((getMissionLayerEntities "Power Plant Lamps") select 0);
};

// Display the mission start text
[] spawn {
	sleep 5;
	[parseText format ["<t align='right' size='1.6'><t font='PuristaBold' size='1.8'>%1<br/></t>%2<br/>%3</t>",
		toUpper "Return to Sender", 
		"by Superxpdude", 
		"16:00:00"],
		true,
		nil,
		10
	] call BIS_fnc_textTiles;
};

// Music event handler for the boss fight
sxp_music_repeat_ID = addMusicEventHandler ["MusicStop", {
	if ((_this select 0) == "wingman_peacekeeper_2") then {
		playMusic (_this select 0);
	};
}];

// Start the music if the player JIPs
if (missionNamespace getVariable ["SXP_boss_music_active", false]) then {
	playMusic "wingman_peacekeeper_2";
};

// Set music volume to maximum
0 fadeMusic 1;

if (_jip) then {
	_player setPos (getMarkerPos ["respawn_west", true]);
};

// HoldActions for task objectives
if (hasInterface) then {
	[
		fob_laptop,
		"Disable Communications Jammer",
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",
		"((_this distance _target) < 5) && ('task_fob' call BIS_fnc_taskExists) && !('task_fob' call BIS_fnc_taskCompleted)",
		"((_caller distance _target) < 5) && ('task_fob' call BIS_fnc_taskExists) && !('task_fob' call BIS_fnc_taskCompleted)",
		{},
		{},
		{["fob_captured"] remoteExec ["SXP_fnc_updateTask", 2];},
		{},
		[],
		5,
		200,
		false
	] call BIS_fnc_holdActionAdd;

	[
		intel_west_laptop,
		"Download access code",
		"media\holdActions\holdAction_usb.paa",
		"media\holdActions\holdAction_usb.paa",
		"((_this distance _target) < 5) && ('task_intel_west' call BIS_fnc_taskExists) && !('task_intel_west' call BIS_fnc_taskCompleted)",
		"((_caller distance _target) < 5) && ('task_intel_west' call BIS_fnc_taskExists) && !('task_intel_west' call BIS_fnc_taskCompleted)",
		{},
		{},
		{["intel_west_activated"] remoteExec ["SXP_fnc_updateTask", 2];},
		{},
		[],
		5,
		200,
		false
	] call BIS_fnc_holdActionAdd;
	
	[
		intel_east_laptop,
		"Download access code",
		"media\holdActions\holdAction_usb.paa",
		"media\holdActions\holdAction_usb.paa",
		"((_this distance _target) < 5) && ('task_intel_east' call BIS_fnc_taskExists) && !('task_intel_east' call BIS_fnc_taskCompleted)",
		"((_caller distance _target) < 5) && ('task_intel_east' call BIS_fnc_taskExists) && !('task_intel_east' call BIS_fnc_taskCompleted)",
		{},
		{},
		{["intel_east_activated"] remoteExec ["SXP_fnc_updateTask", 2];},
		{},
		[],
		5,
		200,
		false
	] call BIS_fnc_holdActionAdd;
	
	[
		research_base_laptop,
		"Send power surge",
		"media\holdActions\holdAction_alert.paa",
		"media\holdActions\holdAction_alert.paa",
		"((_this distance _target) < 5) && ('task_research_base' call BIS_fnc_taskExists) && !('task_research_base' call BIS_fnc_taskCompleted) && (({!(_x call BIS_fnc_taskCompleted)} count ['task_fob','task_intel_west','task_intel_east','task_power_plant']) <= 0)",
		"((_caller distance _target) < 5) && ('task_research_base' call BIS_fnc_taskExists) && !('task_research_base' call BIS_fnc_taskCompleted) && (({!(_x call BIS_fnc_taskCompleted)} count ['task_fob','task_intel_west','task_intel_east','task_power_plant']) <= 0)",
		{},
		{},
		{["research_base_activated"] remoteExec ["SXP_fnc_updateTask", 2];},
		{},
		[],
		5,
		200,
		false
	] call BIS_fnc_holdActionAdd;
	
	[
		boss_calibrate_laptop,
		"Recalibrate Daybreaker",
		"media\holdActions\holdAction_alert.paa",
		"media\holdActions\holdAction_alert.paa",
		"((_this distance _target) < 5) && ('task_calibrate_railgun' call BIS_fnc_taskExists) && !('task_calibrate_railgun' call BIS_fnc_taskCompleted)",
		"((_caller distance _target) < 5) && ('task_calibrate_railgun' call BIS_fnc_taskExists) && !('task_calibrate_railgun' call BIS_fnc_taskCompleted)",
		{},
		{},
		{["railgun_calibrated"] remoteExec ["SXP_fnc_updateTask", 2];},
		{},
		[],
		15,
		200,
		false
	] call BIS_fnc_holdActionAdd;
	
	[
		boss_railgun_laptop,
		"Aim Daybreaker",
		"media\holdActions\holdAction_alert.paa",
		"media\holdActions\holdAction_alert.paa",
		"((_this distance _target) < 5) && ('task_aim_railgun' call BIS_fnc_taskExists) && !('task_aim_railgun' call BIS_fnc_taskCompleted) && !(missionNamespace getVariable ['railgun_rotating',false])",
		"((_caller distance _target) < 5) && ('task_aim_railgun' call BIS_fnc_taskExists) && !('task_aim_railgun' call BIS_fnc_taskCompleted) && !(missionNamespace getVariable ['railgun_rotating',false])",
		{},
		{},
		{[] remoteExec ["SXP_fnc_rotateRailgun", 2];},
		{},
		[],
		5,
		200,
		false
	] call BIS_fnc_holdActionAdd;
	
	[
		boss_railgun_laptop,
		"Fire Daybreaker",
		"media\holdActions\holdAction_alert.paa",
		"media\holdActions\holdAction_alert.paa",
		"((_this distance _target) < 5) && ('task_fire_railgun' call BIS_fnc_taskExists) && !('task_fire_railgun' call BIS_fnc_taskCompleted)",
		"((_caller distance _target) < 5) && ('task_fire_railgun' call BIS_fnc_taskExists) && !('task_fire_railgun' call BIS_fnc_taskCompleted)",
		{},
		{},
		{["railgun_fired"] remoteExec ["SXP_fnc_updateTask", 2];},
		{},
		[],
		5,
		200,
		false
	] call BIS_fnc_holdActionAdd;
	
	{
		[
			_x select 0,
			"Transfer power to Daybreaker",
			"media\holdActions\holdAction_power.paa",
			"media\holdActions\holdAction_power.paa",
			format ["((_this distance _target) < 5) && ('task_railgun_power_%1' call BIS_fnc_taskExists) && !('task_railgun_power_%1' call BIS_fnc_taskCompleted)",_x select 1],
			format ["((_caller distance _target) < 5) && ('task_railgun_power_%1' call BIS_fnc_taskExists) && !('task_railgun_power_%1' call BIS_fnc_taskCompleted)",_x select 1],
			{},
			{},
			{params ["_target", "_caller", "_actionId", "_arguments"]; [format ["railgun_power_%1",_arguments select 1]] remoteExec ["SXP_fnc_updateTask", 2];},
			{},
			_x,
			2,
			200,
			false
		] call BIS_fnc_holdActionAdd;
	} forEach [
		[railgun_power_sw_1,1],
		[railgun_power_sw_2,2],
		[railgun_power_sw_3,3],
		[railgun_power_sw_4,4],
		[railgun_power_sw_5,5],
		[railgun_power_sw_6,6],
		[railgun_power_sw_7,7]
	];
};